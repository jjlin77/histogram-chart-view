# HistogramChartView

#### 介绍
一个自定义的直方图View，横坐标是时间，可以按照 天，周，月来进行统计和绘制。


#### 安装教程

直接导入到自己的工程中即可。

#### 使用说明

1.list数据输入，list的格式定义为：从今天到历史，每一天的数据，打比方：

list.get(0): 表示今天的数据；
list.get(1): 表示昨天的数据；
... ...
list.get(100): 表示100天前的数据；

然后view可以根据时间来绘制柱状图。可以按天，按周（以每周一为一周的开始，周日为一周的结束），按月进行统计绘制


2.可以设置每页显示多少列数据。

3.xml导入：
<com.xxx.view.HistogramChartView
    android:id="@+id/chartv_smoke"
    android:layout_width="match_parent"
    android:layout_height="@dimen/dp_320"/>


#### 参与贡献

1.mark@vaststargames.com


#演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/160451_b8655625_789526.jpeg "Screenshot_20210505_145712.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/160514_86c725de_789526.jpeg "Screenshot_20210505_145727.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/160530_2472b291_789526.jpeg "Screenshot_20210505_145802.jpg")

